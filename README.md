Boost for Windows
=================

--------------------------------------------------------------------------------

    :::text
                   ___________                                             ___
      ___         /  _____   / ___________  ___________  ___________  ____/  /___
     /   \___    /  /____/ _/ /  _____   / /  _____   / /  ________/ /___   ____/
     \__//   \  /  _____  |  /  /    /  / /  /    /  / /  /_______      /  /
    /   \\___/ /  /    /  / /  /    /  / /  /    /  / /_______   /     /  /
    \___/     /  /____/  / /  /____/  / /  /____/  / ________/  /     /  /___
             /__________/ /__________/ /__________/ /__________/     /______/

Table of Contents
-----------------

--------------------------------------------------------------------------------

  - [Description](#markdown-header-description)
  - [Platforms](#markdown-header-platforms)
  - [Toolchains](#markdown-header-toolchains)
  - [Installation](#markdown-header-installation)
  - [Pitfalls](#markdown-header-pitfalls)
      - [`cc1plus.exe` Crash on Large Precompiled Header](#markdown-header-cc1plusexe-crash-on-large-precompiled-header)
      - [No Working Python Interpreter Found](#markdown-header-no-working-python-interpreter-found)
  - [Downloads](#markdown-header-downloads)

Description
-----------

--------------------------------------------------------------------------------

It was never easy to build [Boost][Wikipedia/Boost] on Windows platform as there
are several subtleties and pitfalls, which might be difficult and/or not
immediately obvious how to fix, especially for novices.

By far the most popular toolchain choices for Windows are
[Visual C++][Wikipedia/Visual C++] and [MinGW][Wikipedia/MinGW] (which is
essentially [GCC][Wikipedia/GCC] for Windows). Personally, I prefer MinGW over
Visual C++, and I strongly suggest all the newcomers to start with MinGW and
learn how to be fast and furious with it in everyday workflow. Here is the
objective summary why:

  - GCC is cross-platform: Windows and all platforms from Unix OS family are
    supported, while Visual C++ targets only Microsoft platforms;
  - GCC is known to have much better [C++ standard][] compliance; Furthermore,
    currently it implements much more features of the most recent
    [C++11 standard][Wikipedia/C++11] than Visual C++ does;
  - GCC has incredible amount of back ends, i.e. code generators for a wide
    variety of processors; Visual C++ only targets Intel x86 and x64
    architectures.

As a result, this project focuses more on MinGW rather than Visual C++. Anyway,
building Boost with Visual C++ is somewhat less painful, and there are plenty of
guides how to do it, including the official [Getting Started on Windows][]
guide. Nevertheless, I might add some details on Visual C++ in future.

In these latter days, [MinGW][] project was very slow on improvements, and does
not seem to be actively developed. However, another promising project has forked
from it: [MinGW-w64][], which aims to bring bleeding-edge GCC features for both
x86 and x64 Windows. As of today, MinGW-w64 is actively developed and feels to
be pretty mature to use in production.

To conclude, the goal of this project is to simplify the process of configuring,
building, and installing Boost on Windows. By the way, **out-of-source** builds
are favored.

Platforms
---------

-------------------------------------------------------------------------------

  - Windows 2000;
  - Windows XP;
  - Windows Vista;
  - Windows 7;
  - Windows 8.

Toolchains
----------

-------------------------------------------------------------------------------

  - Borland C++ Builder;
  - Comeau Computing C/C++;
  - Intel C++ Compiler;
  - MetroWerks CodeWarrior C/C++;
  - MinGW;
  - MinGW-w64;
  - Visual C++.

Installation
------------

-------------------------------------------------------------------------------

 1. Obtain [Boost source code][];
 2. Unpack the source tree from the archive to any preferred directory, let's
    further call it `<source-dir>`;
 3. Users of MinGW/MinGW-w64 who wish to build [Boost.Math][] refer to
    [`cc1plus.exe` Crash on Large Precompiled Header](#markdown-header-cc1plusexe-crash-on-large-precompiled-header),
    otherwise proceed to the next step;
 4. Those who wish to build [Boost.Python][] refer to
    [No Working Python Interpreter Found](#markdown-header-no-working-python-interpreter-found),
    otherwise proceed to the next step;
 5. Decide on build directory, and let's refer to this directory as
    `<build-dir>`;

    **NOTE:** You can skip this step. In this case, `install-boost.py` will
    create **temporary** build directory automatically, and you don't have to
    worry about its deletion.

 5. Consider where you want the final installation to end up, let's denote this
    directory by `<install-prefix>`;
 6. Start your favorite shell and invoke:

        :::text
        install-boost.py -source-dir=<source-dir>
                         [-build-dir=<build-dir>]
                         -install-prefix=<install-prefix>
                         -toolchain=(borland|como|gcc|gcc-nocygwin|intel-win32|metrowerks|mingw|msvc|vc7|vc8|vc9|vc10|vc11)
                         -address-model=(32|64)
                         -variant=(release|debug)
                         [-c-flags=<...>]
                         [-cxx-flags=<...>]
                         [-link-flags=<...>]
                         [-verbose]

Pitfalls
--------

--------------------------------------------------------------------------------

### `cc1plus.exe` Crash on Large Precompiled Header

All MinGW distributions have a known issue: insufficiency of available stack
for `cc1plus.exe` to handle large
[precompiled headers][Wikipedia/Precompiled header], which ultimately results
in crash and termination of compilation process. Indeed, this is very
unfortunate since a lot of 3rd party libraries with heavily templatized code
rely on precompiled headers, in order to speed up compilation times, and Boost
is not an exception here.

In particular, Boost.Math is one of the Boost libraries, which relies on a large
precompiled header during its compilation. [Boost.Build][], the primary build
system of Boost (which is used to compile Boost itself), by default injects the
`BOOST_BUILD_PCH_ENABLED` definition during the Boost.Math library compilation.
This definition indicates that this large precompiled header should be used
during the compilation of Boost.Math, which is what we have to disable, in order
to avoid `cc1plus.exe` crash, otherwise it is impossible to build Boost.Math.

To do that, open `<source-dir>/tools/build/v2/tools/pch.jam` and change the
following:

    :::text
    return [ generators.add-usage-requirements $(r)
           : <define>BOOST_BUILD_PCH_ENABLED ] ;

to the following:

    :::text
    return [ generators.add-usage-requirements $(r) ] ;

Now compilation of Boost.Math will run smoothly, but it is worth mentioning
that it does not come for free. In other words, as you've probably guessed
already, the compilation process of Boost.Math will run much slower now. At the
end of the day, it would be great if MinGW developers would have this issue
fixed finally.

### No Working Python Interpreter Found

**NOTE:** This issue was finally resolved starting from version `1.54.0`. If
you're messing around with previous versions, then keep reading, otherwise skip
this section.

Here is the annoying warning that you will most likely experience on Windows:

    :::text
    warning: No python installation configured and autoconfiguration
    note: failed.  See http://www.boost.org/libs/python/doc/building.html
    note: for configuration instructions or pass --without-python to
    note: suppress this message and silently skip all Boost.Python targets

If you run `b2 --debug-configuration`, you'd probably see the following crap:

    :::text
    notice: [python-cfg] Configuring python...
    notice: [python-cfg] Registry indicates Python 2.7 installed at "D:\Applications\Python 2.7.3\"
    notice: [python-cfg] Checking interpreter command "python"...
    notice: [python-cfg] running command 'DIR /-C /A:S "D:\Applications\Python 2.7.3\python.exe" 2>&1'
    notice: [python-cfg] running command '"python" -c "from sys import *;
                                                       print('version=%d.%d\nplatform=%s\nprefix=%s\nexec_prefix=%s\nexecutable=%s' %
                                                       (version_info[0],version_info[1],platform,prefix,exec_prefix,executable))" 2>&1'
    notice: [python-cfg] ...does not invoke a working interpreter
    notice: [python-cfg] Checking interpreter command "D:\Applications\Python 2.7.3\python"...
    notice: [python-cfg] running command 'DIR /-C /A:S "D:\Applications\Python 2.7.3\python.exe" 2>&1'
    notice: [python-cfg] running command '"D:\Applications\Python 2.7.3\python" -c "from sys import *;
                                                                                    print('version=%d.%d\nplatform=%s\nprefix=%s\nexec_prefix=%s\nexecutable=%s' %
                                                                                    (version_info[0],version_info[1],platform,prefix,exec_prefix,executable))" 2>&1'
    notice: [python-cfg] ...does not invoke a working interpreter
    notice: [python-cfg] No working Python interpreter found.
    notice: [python-cfg] running command 'DIR /-C /A:S "D:\Applications\Python 2.7.3\python.exe" 2>&1'
    notice: [python-cfg] falling back to "python"
    notice: [python-cfg] Details of this Python configuration:
    notice: [python-cfg]   interpreter command: "python"
    notice: [python-cfg]   include path: "Include"
    notice: [python-cfg]   library path: "\libs"
    notice: [python-cfg]   DLL search path: "<empty>"

This bug has been there for ages by now. A workaround is to open
`<source-dir>/tools/build/v2/tools/python.jam` and remove completely the
following snippet:

    :::text
    # Invoke Python and ask it for all those values.
    if [ version.check-jam-version 3 1 17 ] || ( [ os.name ] != NT )
    {
        # Prior to version 3.1.17 Boost Jam's SHELL command did not support
        # quoted commands correctly on Windows. This means that on that
        # platform we do not support using a Python command interpreter
        # executable whose path contains a space character.
        python-cmd = \"$(python-cmd)\" ;
    }

Now [Python][Wikipedia/Python] will be properly detected by Boost.Build, just
make sure that you have the directory containing `python.exe` in the `PATH`
environment variable.

Downloads
---------

-------------------------------------------------------------------------------

For those who are lazy or impatient, like me `:)`, enjoy prebuilt and
ready-to-use binary distributions of Boost for various toolchains:

  - [MinGW-w64/4.8.1/x86/POSIX/DWARF][]:
      - [1.54.0][Downloads/Boost/1.54.0/MinGW-w64/4.8.1/x86/POSIX/DWARF].

  - [MinGW-w64/4.8.1/x64/POSIX/SEH][]:
      - [1.54.0][Downloads/Boost/1.54.0/MinGW-w64/4.8.1/x64/POSIX/SEH].

[Wikipedia/Boost]:                                              http://en.wikipedia.org/wiki/Boost_(C%2B%2B_libraries)
[Wikipedia/Visual C++]:                                         http://en.wikipedia.org/wiki/Visual_C%2B%2B
[Wikipedia/MinGW]:                                              http://en.wikipedia.org/wiki/MinGW
[Wikipedia/GCC]:                                                http://en.wikipedia.org/wiki/GNU_Compiler_Collection
[Wikipedia/C++11]:                                              http://en.wikipedia.org/wiki/C%2B%2B11
[Wikipedia/Precompiled header]:                                 http://en.wikipedia.org/wiki/Precompiled_header
[Wikipedia/Python]:                                             http://en.wikipedia.org/wiki/Python_(programming_language)
[C++ standard]:                                                 http://isocpp.org/std/the-standard
[Getting Started on Windows]:                                   http://www.boost.org/doc/libs/1_53_0/more/getting_started/windows.html
[MinGW]:                                                        http://www.mingw.org/
[MinGW-w64]:                                                    http://mingw-w64.sourceforge.net/
[Boost source code]:                                            http://sourceforge.net/projects/boost/files/boost/
[Boost.Math]:                                                   http://www.boost.org/doc/libs/1_53_0/libs/math/doc/html/index.html
[Boost.Python]:                                                 http://www.boost.org/doc/libs/1_53_0/libs/python/doc/index.html
[Boost.Build]:                                                  http://www.boost.org/doc/libs/1_53_0/doc/html/bbv2.html
[MinGW-w64/4.8.1/x86/POSIX/DWARF]:                              http://sourceforge.net/projects/mingwbuilds/files/host-windows/releases/4.8.1/32-bit/threads-posix/dwarf/
[MinGW-w64/4.8.1/x64/POSIX/SEH]:                                http://sourceforge.net/projects/mingwbuilds/files/host-windows/releases/4.8.1/64-bit/threads-posix/seh/
[Downloads/Boost/1.54.0/MinGW-w64/4.8.1/x86/POSIX/DWARF]:       ../../downloads/boost-1.54.0-mingw-w64-4.8.1-x86-posix-dwarf.zip
[Downloads/Boost/1.54.0/MinGW-w64/4.8.1/x64/POSIX/SEH]:         ../../downloads/boost-1.54.0-mingw-w64-4.8.1-x64-posix-seh.zip
